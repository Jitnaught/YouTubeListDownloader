﻿using System.IO;
using System.Reflection;
using IniParser;

namespace YouTubeListDownloader
{
    public class INI
    {
        internal static string DefaultAddTextFileDir, DefaultDownloadDir;

        static string iniPath;

        internal static void LoadSettings()
        {
			iniPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "settings.ini");

			if (File.Exists(iniPath))
			{
				var ini = new FileIniDataParser().ReadFile(iniPath);

				DefaultAddTextFileDir = ini.Sections.GetSectionData("SETTINGS").Keys.GetKeyData("DEFAULT_ADD_TEXT_FILE_DIR").Value;
				DefaultDownloadDir = ini.Sections.GetSectionData("SETTINGS").Keys.GetKeyData("DEFAULT_DOWNLOAD_DIR").Value;
			}
			else
			{
				DefaultAddTextFileDir = DefaultDownloadDir = "";

				WriteSettings(DefaultAddTextFileDir, DefaultDownloadDir);
			}
        }

        internal static void WriteSettings(string defaultAddTextFileDir, string defaultDownloadDir)
        {
            DefaultAddTextFileDir = defaultAddTextFileDir;
            DefaultDownloadDir = defaultDownloadDir;

			var iniData = new IniParser.Model.IniData();
			var sectionData = new IniParser.Model.SectionData("SETTINGS");
			sectionData.Keys.AddKey("DEFAULT_ADD_TEXT_FILE_DIR", defaultAddTextFileDir);
			sectionData.Keys.AddKey("DEFAULT_DOWNLOAD_DIR", defaultDownloadDir);
			iniData.Sections.Add(sectionData);

			new FileIniDataParser().WriteFile(iniPath, iniData);
        }
    }
}
