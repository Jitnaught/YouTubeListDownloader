﻿using System;
using System.Diagnostics;
using System.IO;
using YouTubeListDownloader;

public partial class MainWindow : Gtk.Window
{
    const string BASE_YT_LINK = "http://www.youtube.com/";
    const string WATCH_ID = "watch?v=";
    const string PLAYLIST_ID = "playlist?list=";

    SettingsWindow settingsWindow = null;

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();

		INI.LoadSettings();

        textviewDownloadDir.Buffer.Text = INI.DefaultDownloadDir;
    }

    protected void OnDeleteEvent(object sender, Gtk.DeleteEventArgs a)
    {
        Gtk.Application.Quit();
        a.RetVal = true;
    }

    protected void buttonClear_Clicked(object sender, EventArgs e)
    {
        if (MessageBox.Show("Are you sure you want to clear?", Gtk.MessageType.Question, Gtk.ButtonsType.YesNo) == Gtk.ResponseType.Yes)
        {
            textviewList.Buffer.Clear();
        }
    }

    protected void buttonDownload_Clicked(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(textviewFormats.Buffer.Text))
		{
			MessageBox.Show("No format inputed.");
			return;
		}

        if (string.IsNullOrWhiteSpace(textviewDownloadDir.Buffer.Text) || !Directory.Exists(textviewDownloadDir.Buffer.Text))
		{
			MessageBox.Show("Download directory could not be found.");
			return;
		}

        if (string.IsNullOrWhiteSpace(textviewList.Buffer.Text))
		{
			MessageBox.Show("No YouTube links provided.");
			return;
		}

		string ytLinks = "";

		foreach (string line in textviewList.Buffer.Text.Split('\n'))
		{
			if (!string.IsNullOrWhiteSpace(line) && line != Environment.NewLine)
			{
				string lineLower = line.ToLower();

				if (lineLower.Contains("youtube."))
				{
                    int watchIndex = -1, listIndex = -1;

                    if ((watchIndex = lineLower.IndexOf(WATCH_ID)) > -1 ||
                        (listIndex = lineLower.IndexOf(PLAYLIST_ID)) > -1)
                    {
                        string link = line;
                        int ampersandIndex = -1;

						//extra parameters
						if ((ampersandIndex = lineLower.IndexOf('&', (watchIndex == -1 ? listIndex : watchIndex))) > -1)
                        {
                            //remove unnessecary parameters in link
                            string id = "";

                            if (watchIndex > -1)
                            {
                                watchIndex += WATCH_ID.Length;

								//ids are case-sentisitive, so make sure to use non-lowercased link
								id = line.Substring(watchIndex, ampersandIndex - watchIndex);

                                link = BASE_YT_LINK + WATCH_ID + id;
                            }
                            else if (listIndex > -1)
                            {
								listIndex += PLAYLIST_ID.Length;

								id = line.Substring(listIndex, ampersandIndex - listIndex);

								link = BASE_YT_LINK + PLAYLIST_ID + id;
                            }
                        }

                        ytLinks += link + " ";
                    }
				}
			}
		}

		if (ytLinks != "")
		{
			ytLinks = ytLinks.Substring(0, ytLinks.Length - 1); //remove space at end

            string command = " -f " + textviewFormats.Buffer.Text + " --output \"" + textviewDownloadDir.Buffer.Text + (textviewDownloadDir.Buffer.Text.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) ? "" : System.IO.Path.DirectorySeparatorChar.ToString()) + "%(title)s.%(ext)s\" " + ytLinks;

			int platform = (int)Environment.OSVersion.Platform;

			ProcessStartInfo procInfo = new ProcessStartInfo();
			procInfo.UseShellExecute = false;

            if (platform == 4 || platform == 128) //unix
            {
				procInfo.FileName = "gnome-terminal";
                procInfo.Arguments = "-e 'youtube-dl " + command + "'";
            }
            else if (platform == 6) //mac
			{
                procInfo.FileName = "youtube-dl " + command;
            }
            else //windows
            {
                //starting youtube-dl this way so that the prompt won't close when youtube-dl is done
                procInfo.FileName = "CMD.EXE";
                procInfo.Arguments = "/K \"youtube-dl.exe " + command.Replace("\"", "\"\"") + "\"";
            }

            Process.Start(procInfo);
		}
    }

    protected void buttonSettings_Clicked(object sender, EventArgs e)
    {
        if (settingsWindow == null)
        {
            settingsWindow = new SettingsWindow();
            settingsWindow.Show();

            this.Sensitive = false;

            settingsWindow.KeepAbove = true;
            settingsWindow.Destroyed += (s, args) =>
            {
                this.Sensitive = true;
                settingsWindow = null;
            };
        }
    }

    protected void buttonAddFile_Clicked(object sender, EventArgs e)
    {
		var dialog = new Gtk.FileChooserDialog("Select text file", null, Gtk.FileChooserAction.Open);

		dialog.AddButton(Gtk.Stock.Cancel, Gtk.ResponseType.Cancel);
		dialog.AddButton(Gtk.Stock.Open, Gtk.ResponseType.Ok);

        dialog.SetCurrentFolder(INI.DefaultAddTextFileDir);

        this.Sensitive = false;

		if ((Gtk.ResponseType)dialog.Run() == Gtk.ResponseType.Ok)
		{
			var stream = File.OpenText(dialog.Filename);

			while (stream.Peek() >= 0)
			{
				string line = stream.ReadLine();

				if (!string.IsNullOrWhiteSpace(line) && line != Environment.NewLine)
				{
					string lineLower = line.ToLower();

					if (lineLower.Contains("youtube.") && lineLower.Contains("watch?v="))
					{
						textviewList.Buffer.Text += line + Environment.NewLine;
					}
				}
			}
		}

        this.Sensitive = true;

		dialog.Destroy();
    }

    protected void buttonBrowseDownloadDir_Clicked(object sender, EventArgs e)
    {
        var dialog = new Gtk.FileChooserDialog("Select folder", null, Gtk.FileChooserAction.SelectFolder);

        dialog.AddButton(Gtk.Stock.Cancel, Gtk.ResponseType.Cancel);
        dialog.AddButton(Gtk.Stock.Open, Gtk.ResponseType.Ok);

        if (string.IsNullOrWhiteSpace(textviewDownloadDir.Buffer.Text) || !Directory.Exists(textviewDownloadDir.Buffer.Text))
        {
            dialog.SetCurrentFolder(INI.DefaultDownloadDir);
        }
        else
        {
            dialog.SetCurrentFolder(textviewDownloadDir.Buffer.Text);
        }

        this.Sensitive = false;

		if ((Gtk.ResponseType)dialog.Run() == Gtk.ResponseType.Ok)
		{
			textviewDownloadDir.Buffer.Text = dialog.Filename;
		}

        this.Sensitive = true;

        dialog.Destroy();
    }
}
