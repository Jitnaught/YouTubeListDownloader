﻿using System;
using System.IO;
using Gtk;

namespace YouTubeListDownloader
{
    public partial class SettingsWindow : Window
    {
        public SettingsWindow() :
                base(WindowType.Toplevel)
        {
            this.Build();

            textviewAddTextFileDir.Buffer.Text = INI.DefaultAddTextFileDir;
            textviewDownloadDir.Buffer.Text = INI.DefaultDownloadDir;
        }

        protected void buttonSave_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textviewAddTextFileDir.Buffer.Text) || !Directory.Exists(textviewAddTextFileDir.Buffer.Text))
            {
                MessageBox.Show("Default browse directory could not be located.");
                return;
            }

            if (string.IsNullOrWhiteSpace(textviewDownloadDir.Buffer.Text) || !Directory.Exists(textviewDownloadDir.Buffer.Text))
            {
                MessageBox.Show("Default download directory could not be located.");
                return;
            }

			INI.WriteSettings(textviewAddTextFileDir.Buffer.Text, textviewDownloadDir.Buffer.Text);

            this.Destroy();
        }

        protected void buttonCancel_Clicked(object sender, EventArgs e)
        {
            this.Destroy();
        }

        protected void buttonBrowseDefaultBrowse_Clicked(object sender, EventArgs e)
        {
			var dialog = new FileChooserDialog("Select folder", null, FileChooserAction.SelectFolder);

			dialog.AddButton(Stock.Cancel, ResponseType.Cancel);
			dialog.AddButton(Stock.Open, ResponseType.Ok);

            dialog.SetCurrentFolder(INI.DefaultAddTextFileDir);

            this.Sensitive = false;

			if ((ResponseType)dialog.Run() == ResponseType.Ok)
			{
                textviewAddTextFileDir.Buffer.Text = dialog.Filename;
			}

            this.Sensitive = true;

            dialog.Destroy();
        }

        protected void buttonBrowseDefaultDownload_Clicked(object sender, EventArgs e)
        {
			var dialog = new FileChooserDialog("Select folder", null, FileChooserAction.SelectFolder);

			dialog.AddButton(Stock.Cancel, ResponseType.Cancel);
			dialog.AddButton(Stock.Open, ResponseType.Ok);

			dialog.SetCurrentFolder(INI.DefaultDownloadDir);

            this.Sensitive = false;

			if ((ResponseType)dialog.Run() == ResponseType.Ok)
			{
				textviewDownloadDir.Buffer.Text = dialog.Filename;
			}

            this.Sensitive = true;

            dialog.Destroy();
        }
    }
}
