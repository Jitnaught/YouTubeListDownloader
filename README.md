Sends a list of YouTube video/playlist links to youtube-dl. Written in C# using Mono (.NET Framework version 4.6.1), GTK# 2.12, [ini-parser](https://github.com/rickyah/ini-parser), and youtube-dl.

Requirements:

* Linux: mono, gtk-sharp2, youtube-dl
* Windows: .NET 4.6.1, [GTK#](http://www.mono-project.com/download/#download-win), youtube-dl.exe extracted to within YouTubeListDownloader's folder
* Mac: (untested) [Mono](http://www.mono-project.com/download/#download-mac), youtube-dl

[Download.](https://github.com/Jitnaught/YouTubeListDownloader-Mono/releases)

P.S. If you are building from source using MonoDevelop on Linux make sure to remove the Mono.Posix reference, otherwise the program won't run under Windows.

![Main window](https://i.imgur.com/fdYWzMm.png)

![Settings window](https://i.imgur.com/R9YS7cj.png)