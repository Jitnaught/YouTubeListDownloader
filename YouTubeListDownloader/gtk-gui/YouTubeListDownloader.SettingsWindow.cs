
// This file has been generated by the GUI designer. Do not modify.
namespace YouTubeListDownloader
{
	public partial class SettingsWindow
	{
		private global::Gtk.VBox vbox3;

		private global::Gtk.HBox hbox4;

		private global::Gtk.Label label3;

		private global::Gtk.TextView textviewAddTextFileDir;

		private global::Gtk.Button buttonBrowseDefaultBrowse;

		private global::Gtk.HBox hbox5;

		private global::Gtk.Label label4;

		private global::Gtk.TextView textviewDownloadDir;

		private global::Gtk.Button buttonBrowseDefaultDownload;

		private global::Gtk.HBox hbox6;

		private global::Gtk.Button buttonCancel;

		private global::Gtk.Button buttonSave;

		protected virtual void Build()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget YouTubeListDownloader.SettingsWindow
			this.Name = "YouTubeListDownloader.SettingsWindow";
			this.Title = "Settings";
			this.Icon = global::Stetic.IconLoader.LoadIcon(this, "gtk-go-down", global::Gtk.IconSize.Dialog);
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			// Container child YouTubeListDownloader.SettingsWindow.Gtk.Container+ContainerChild
			this.vbox3 = new global::Gtk.VBox();
			this.vbox3.Name = "vbox3";
			this.vbox3.Spacing = 6;
			// Container child vbox3.Gtk.Box+BoxChild
			this.hbox4 = new global::Gtk.HBox();
			this.hbox4.Name = "hbox4";
			this.hbox4.Spacing = 6;
			// Container child hbox4.Gtk.Box+BoxChild
			this.label3 = new global::Gtk.Label();
			this.label3.Name = "label3";
			this.label3.LabelProp = "Default add text file directory";
			this.hbox4.Add(this.label3);
			global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.hbox4[this.label3]));
			w1.Position = 0;
			w1.Expand = false;
			w1.Fill = false;
			// Container child hbox4.Gtk.Box+BoxChild
			this.textviewAddTextFileDir = new global::Gtk.TextView();
			this.textviewAddTextFileDir.CanFocus = true;
			this.textviewAddTextFileDir.Name = "textviewAddTextFileDir";
			this.hbox4.Add(this.textviewAddTextFileDir);
			global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.hbox4[this.textviewAddTextFileDir]));
			w2.Position = 1;
			// Container child hbox4.Gtk.Box+BoxChild
			this.buttonBrowseDefaultBrowse = new global::Gtk.Button();
			this.buttonBrowseDefaultBrowse.HeightRequest = 25;
			this.buttonBrowseDefaultBrowse.CanFocus = true;
			this.buttonBrowseDefaultBrowse.Name = "buttonBrowseDefaultBrowse";
			this.buttonBrowseDefaultBrowse.UseUnderline = true;
			this.buttonBrowseDefaultBrowse.Label = "...";
			this.hbox4.Add(this.buttonBrowseDefaultBrowse);
			global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.hbox4[this.buttonBrowseDefaultBrowse]));
			w3.Position = 2;
			w3.Expand = false;
			w3.Fill = false;
			this.vbox3.Add(this.hbox4);
			global::Gtk.Box.BoxChild w4 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox4]));
			w4.Position = 0;
			w4.Expand = false;
			w4.Fill = false;
			// Container child vbox3.Gtk.Box+BoxChild
			this.hbox5 = new global::Gtk.HBox();
			this.hbox5.Name = "hbox5";
			this.hbox5.Spacing = 6;
			// Container child hbox5.Gtk.Box+BoxChild
			this.label4 = new global::Gtk.Label();
			this.label4.Name = "label4";
			this.label4.LabelProp = "Default download directory";
			this.hbox5.Add(this.label4);
			global::Gtk.Box.BoxChild w5 = ((global::Gtk.Box.BoxChild)(this.hbox5[this.label4]));
			w5.Position = 0;
			w5.Expand = false;
			w5.Fill = false;
			// Container child hbox5.Gtk.Box+BoxChild
			this.textviewDownloadDir = new global::Gtk.TextView();
			this.textviewDownloadDir.CanFocus = true;
			this.textviewDownloadDir.Name = "textviewDownloadDir";
			this.hbox5.Add(this.textviewDownloadDir);
			global::Gtk.Box.BoxChild w6 = ((global::Gtk.Box.BoxChild)(this.hbox5[this.textviewDownloadDir]));
			w6.Position = 1;
			// Container child hbox5.Gtk.Box+BoxChild
			this.buttonBrowseDefaultDownload = new global::Gtk.Button();
			this.buttonBrowseDefaultDownload.HeightRequest = 25;
			this.buttonBrowseDefaultDownload.CanFocus = true;
			this.buttonBrowseDefaultDownload.Name = "buttonBrowseDefaultDownload";
			this.buttonBrowseDefaultDownload.UseUnderline = true;
			this.buttonBrowseDefaultDownload.Label = "...";
			this.hbox5.Add(this.buttonBrowseDefaultDownload);
			global::Gtk.Box.BoxChild w7 = ((global::Gtk.Box.BoxChild)(this.hbox5[this.buttonBrowseDefaultDownload]));
			w7.Position = 2;
			w7.Expand = false;
			w7.Fill = false;
			this.vbox3.Add(this.hbox5);
			global::Gtk.Box.BoxChild w8 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox5]));
			w8.Position = 1;
			w8.Expand = false;
			w8.Fill = false;
			// Container child vbox3.Gtk.Box+BoxChild
			this.hbox6 = new global::Gtk.HBox();
			this.hbox6.Name = "hbox6";
			this.hbox6.Spacing = 6;
			// Container child hbox6.Gtk.Box+BoxChild
			this.buttonCancel = new global::Gtk.Button();
			this.buttonCancel.CanFocus = true;
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseUnderline = true;
			this.buttonCancel.Label = "Cancel";
			this.hbox6.Add(this.buttonCancel);
			global::Gtk.Box.BoxChild w9 = ((global::Gtk.Box.BoxChild)(this.hbox6[this.buttonCancel]));
			w9.PackType = ((global::Gtk.PackType)(1));
			w9.Position = 0;
			w9.Expand = false;
			w9.Fill = false;
			// Container child hbox6.Gtk.Box+BoxChild
			this.buttonSave = new global::Gtk.Button();
			this.buttonSave.CanFocus = true;
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.UseUnderline = true;
			this.buttonSave.Label = "Save";
			this.hbox6.Add(this.buttonSave);
			global::Gtk.Box.BoxChild w10 = ((global::Gtk.Box.BoxChild)(this.hbox6[this.buttonSave]));
			w10.PackType = ((global::Gtk.PackType)(1));
			w10.Position = 1;
			w10.Expand = false;
			w10.Fill = false;
			this.vbox3.Add(this.hbox6);
			global::Gtk.Box.BoxChild w11 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.hbox6]));
			w11.Position = 2;
			w11.Expand = false;
			w11.Fill = false;
			this.Add(this.vbox3);
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.DefaultWidth = 428;
			this.DefaultHeight = 97;
			this.Show();
			this.buttonBrowseDefaultBrowse.Clicked += new global::System.EventHandler(this.buttonBrowseDefaultBrowse_Clicked);
			this.buttonBrowseDefaultDownload.Clicked += new global::System.EventHandler(this.buttonBrowseDefaultDownload_Clicked);
			this.buttonSave.Clicked += new global::System.EventHandler(this.buttonSave_Clicked);
			this.buttonCancel.Clicked += new global::System.EventHandler(this.buttonCancel_Clicked);
		}
	}
}
